# Angular2 Wizard Switch

This project for Angular 2 (currently supported version 8) is a cool switch for opening and closing *HTML* elements such as inputs, textarea, links.
Demo project here:
Live demo example [here](https://angular2-pretty-component-demo.herokuapp.com/switch).

## Features
#### Support user templates
You can generate html templates to inject it in the switch.

#### Using font awesome icons as text of the control
You can use icon as text of the control or using simple text. 

#### Make control is disabled or enabled
You have change disabled/enabled control by index, disabled all and enabled all actions.

#### Width management
You can manage item amd switch width.

#### Custom styles
You can customize class of the controllers or adding your own class.

#### Custom animation
You can customize the duration, delay and animation type.

#### Animation events
You can use start or end animation event.

## Installation

### Dependencies
For the slider to work correctly, the following dependencies are required:
- "@fortawesome/angular-fontawesome": "^0.5.0"
- "@fortawesome/fontawesome-svg-core": "^1.2.22"
- "@fortawesome/free-solid-svg-icons": "^5.10.2"

### How to install
#### With npm
```npm install @kinect-pro/wizard-switch```
#### With yarn
```yarn add @kinect-pro/wizard-switch```

### How to setup
#### Add WizardSwitchModule and BrowserAnimationsModule to need module
##### app.module
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WizardSwitchModule } from '@kinect-pro/wizard-switch';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    Angular2CarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

#### Add switch component to need component and create template items.
##### app.component.html
```
<kp-wizard-switch>
    <input type="text" [kpItemTemplate]="'login'" [control]="'Login'">
    <input type="text" [kpItemTemplate]="'password'" [control]="'Password'">
</kp-wizard-switch>
```
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Using
### Use font awesome icons
More information on using icons can be found [here](https://www.npmjs.com/package/@fortawesome/angular-fontawesome).
Add icons to the component and import them to items array as value of the `control` property.
##### app.component.ts
```
import { Component } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private service: DisableService) {}
  coffeeIcon = faCoffee;
}
```
##### app.component.html
```
<kp-wizard-switch>
    <input type="text" [kpItemTemplate]="'coffee'" [control]="coffeeIcon">
</kp-wizard-switch>
```

### Set custom styles for controls
#### Using default styles
Add your styles into `::ng-deep{}` in needed component style file.
##### app.component.scss
```
::ng-deep {
    .kp-wizard-control {
      span {
        background: green !important;
        color: yellow !important;
      }
    }
}
```

or you can adding global styles in your main style file.

##### styles.scss
```
.kp-wizard-control {
  span {
    background: green !important;
    color: yellow !important;
  }
}
```
*You must use `!important` to overwrite existing styles.

#### Using custom class
Add `[controlStyle]="'your-class-name'"` to `<kp-wizard-switch></kp-wizard-switch>`.
##### app.component.scss
```
::ng-deep {
    .custom-control {
      span {
        background: green;
        color: yellow;
      }
    }
}
```

##### app.component.html
```
<kp-wizard-switch [controlStyle]="'custom-control'">
    // Your templates here
</kp-wizard-switch>
```

### Set custom animation
- duration: change animation duration. Must be integer (milliseconds) or string (seconds, for example '1.4s' or milliseconds, for example '1400ms'). Default value is '250ms'.
- delay: change animation delay. Must be as duration. Default value is 0.
- easing: change animation easing. Must be 'ease', 'easeIn', 'easeInOut', 'easeOut', 'linear' or valid 'cubic-bezier()'. You can create valid curve in generator [here](https://cubic-bezier.com/). Default value is 'easeIn'.
##### app.component.html
```
<kp-wizard-switch
    [animateOptions]="{duration: '1000ms', delay: '300ms', easing: 'cubic-bezier(.17,.67,.88,.1)'}">
    // Your templates here
</kp-wizard-switch>
```

### Using disable actions
Add `ControllService` into needed component
##### app.component.ts
```
import { Component } from '@angular/core';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { ControllService } from '@kinect-pro/wizard-switch';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private service: ControllService) {}

  changeDisabled() {
    this.service.disable(3); // Use this for change disabled option by item index
    // this.service.enableAll(); // Use this for enabled all items
    // this.service.disableAll(); // Use this for disabled all items
  }
}
```

##### app.component.html
Add `[onDisableService]="true"` to component. If you need make some items after initializing use `[disabled]="true"` for kpItemTemplate.
```
<kp-wizard-switch [onDisableService]="true">
  <input type="text" [kpItemTemplate]="'coffee'" [control]="coffeeIcon" [disabled]="true">
</kp-wizard-switch>
<button (click)="changeDisabled()">Change disable option</button>
```

### Using trigger actions
Add `ControllService` into needed component
##### app.component.ts
```
import { Component } from '@angular/core';
import { ControllService } from '@kinect-pro/wizard-switch';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private service: ControllService) {}

  triggering() {
    this.service.trigger('login'); // Use this for triggering login control
    // this.service.closeAll(); // Use this for closing all items
    // this.service.openFirst(); // Use this for opening first item
    // this.service.openLast(); // Use this for opening last item
    // this.service.openNext(); // Use this for opening next item
    // this.service.openPrev(); // Use this for opening previous item
  }
}
```

##### app.component.html
```
<kp-wizard-switch>
  <input type="text" placeholder="login" [kpItemTemplate]="'login'" [control]="'Login'">
  <input type="text" placeholder="password" [kpItemTemplate]="'password'" [control]="'Password'">
</kp-wizard-switch>
<button (click)="triggering()">Login trigger</button>
```

### Width management
#### Items width management
Use `[uniformWidth]` option to change the display style of the width of the elements. For this, pass the boolean parameter (true or false) to the HTML template.

##### app.component.html
```
<kp-wizard-switch [uniformWidth]="false">
    // Your templates here
</kp-wizard-switch>
```
If you choose true, then all elements will have the same value equal to the width of the longest element. For false option every item will have own width. Default value is true.

#### Switch width management
Use `[autoResponsive]` option to change the display style of the width of the switch. For this, pass the boolean parameter (true or false) to the HTML template.

##### app.component.html
```
<kp-wizard-switch
    [uniformWidth]="false"
    [autoResponsive]="true">
  // Your templates here
</kp-wizard-switch>
```
If you choose true, then switch will change width after any animation. For false option width will't change. Default value is false. Use this option, then `[uniformWidth]="false"`.

### Using animation events
You can use your handler functions to start or end the animation.

##### app.component.html
```
<kp-wizard-switch
    (onStartAnimation)="startAnimationCallback()"
    (onEndAnimation)="endAnimationCallback()">
  // Your templates here
</kp-wizard-switch>
```

##### app.component.ts
```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  startAnimationCallback() {
    console.log('Start animation');
  }
  
  endAnimationCallback() {
    console.log('End animation');
  }
}
```

### Manage multiple switches in one component
If you have several switches in one component, you can control all at once, or only certain. To do this, you need to specify a name for each switch `[name]="'yourUniqName'"`, and pass them to control methods for controls.

##### app.component.html
```
<kp-wizard-switch [name]="'first'>
      <input type="text" placeholder="login" [kpItemTemplate]="'login'" [control]="'Login'">
      <input type="text" placeholder="password" [kpItemTemplate]="'password'" [control]="'Password'">
</kp-wizard-switch>

<kp-wizard-switch [name]="'second'>
      <input type="text" placeholder="login" [kpItemTemplate]="'login'" [control]="'Login'">
      <input type="text" placeholder="password" [kpItemTemplate]="'password'" [control]="'Password'">
</kp-wizard-switch>

<kp-wizard-switch [name]="'third'>
      <input type="text" placeholder="login" [kpItemTemplate]="'login'" [control]="'Login'">
      <input type="text" placeholder="password" [kpItemTemplate]="'password'" [control]="'Password'">
</kp-wizard-switch>

<div>
  <button (click)="changeDisabled()">Disable login</button>
  <button (click)="disableAll()">Disable all</button>
  <button (click)="enableAll()">Enable all</button>
</div>
```

##### app.component.ts
```
import { Component } from '@angular/core';
import { ControllService } from '@kinect-pro/wizard-switch';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private service: ControlService) { }

    changeDisabled() {
      this.service.disable('login', ['second', 'third']);
    }
  
    disableAll() {
      this.service.disableAll();
    }
  
    enableAll() {
      this.service.enableAll('second');
    }
}
```
`changeDisabled()` will act only on the second and third switches.
`disableAll()` will act on the all switches, because switchName option is empty.
`enableAll()` will act only on the second switch.

In the `switchName`, you can pass either a string with one name or an array of names. If you pass `[name]="'notFound'"` of a switch that is not in the component, then the control functions **WILL NOT WORK**

## Contact us

