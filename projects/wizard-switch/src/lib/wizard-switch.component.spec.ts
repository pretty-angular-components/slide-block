import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardSwitchComponent } from './wizard-switch.component';

describe('WizardSwitchComponent', () => {
  let component: WizardSwitchComponent;
  let fixture: ComponentFixture<WizardSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
