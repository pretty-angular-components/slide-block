export enum Trigger {
  trigger = 'trigger',
  closeAll = 'closeAll',
  openFirst = 'openFirst',
  openLast = 'openLast',
  openNext = 'openNext',
  openPrev = 'openPrev'
}
