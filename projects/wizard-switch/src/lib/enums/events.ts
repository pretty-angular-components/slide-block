export enum Events {
  onStartAnimation = 'startAnimation',
  onEndAnimation = 'endAnimation'
}

export enum EventStates {
  start = 'start',
  end = 'end',
  click = 'click'
}
