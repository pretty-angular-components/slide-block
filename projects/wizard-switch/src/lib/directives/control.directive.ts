import { Directive,
  HostListener,
  Input,
  Renderer2,
  ElementRef,
  OnInit,
  OnDestroy,
  AfterViewInit } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DisableService } from '../services/disable.service';
import { TriggerService } from '../services/trigger.service';
import { WizardSwitchService } from '../services/wizard-switch.service';
import { ItemTemplateDirective } from './item-template.directive';
import { ControlOptionInterface } from '../models-interfaces/controlOption';

@Directive({
  selector: '[kpControl]',
})
export class ControlDirective implements OnInit, OnDestroy, AfterViewInit {
  @Input() private index;
  @Input() private controlStyle;
  @Input() private switchName;
  @Input() source: ItemTemplateDirective;

  private destroyer$ = new Subject<void>();

  private rect;
  private disableAttrName = 'disabled';

  @HostListener('click') onClick() {
    if (!this.source.disabled) {
      this.switchService.setIndex(this.index);
    }
  }

  constructor(
    private switchService: WizardSwitchService,
    private disableService: DisableService,
    private triggerService: TriggerService,
    private el: ElementRef,
    private render: Renderer2) { }

  ngOnInit() {
    if (this.source.disabled) {
      this.render.setAttribute(this.el.nativeElement, this.disableAttrName, 'true');
    }

    this.disableService.getChangeDisabledOption()
      .pipe(takeUntil(this.destroyer$))
      .subscribe((option: ControlOptionInterface) => {
        if (this.checkOption(option)) {
          this.changeDisable(this.source.disabled);
        }
      });

    this.disableService.getChangeDisabledAll()
      .pipe(takeUntil(this.destroyer$))
      .subscribe((option: ControlOptionInterface) => {
        if (this.checkOption(option)) {
          this.changeDisable(option.value);
        }
      });

    this.triggerService.getTrigger()
      .pipe(takeUntil(this.destroyer$))
      .subscribe((option: ControlOptionInterface) => {
        if (this.checkOption(option) && !this.source.disabled) {
          this.switchService.setIndex(this.index);
        }
      });

    this.render.addClass(this.el.nativeElement, this.controlStyle);
  }

  ngOnDestroy() {
    this.destroyer$.next();
    this.destroyer$.complete();
  }

  ngAfterViewInit() {
    this.rect = this.el.nativeElement.getBoundingClientRect();
  }

  get width() {
    return this.rect.width;
  }

  private disableElem() {
    this.render.setAttribute(this.el.nativeElement, this.disableAttrName, 'true');
    this.source.disabled = true;
  }

  private enableElem() {
    this.render.removeAttribute(this.el.nativeElement, this.disableAttrName);
    this.source.disabled = false;
  }

  private changeDisable(isDisabled) {
    isDisabled ? this.enableElem() : this.disableElem();
  }

  private checkOption(data: ControlOptionInterface) {
    return this.isEquivalentValue(data.value) && this.isEquivalentName(data.switchName);
  }

  private isEquivalentValue(value: string | number | boolean) {
    return (typeof value === 'boolean') || value === this.source.kpItemTemplate || value === this.index;
  }

  private isEquivalentName(switchName: string[]) {
    return !switchName || this.findName(switchName);
  }

  private findName(names: string[]) {
    return !!names.find((name: string) => name === this.switchName);
  }
}
