import { Directive, AfterViewInit, OnInit, Renderer2, ElementRef, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemTemplateDirective } from './item-template.directive';

@Directive({
  selector: '[kpElement]'
})
export class ElementDirective implements OnInit, AfterViewInit {
  private rect;
  @Input() private item: ItemTemplateDirective;
  @Input() private width$: Observable<number>;

  constructor(private render: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    this.generateTemplate();
  }

  ngAfterViewInit(): void {
    this.rect = this.el.nativeElement.getBoundingClientRect();
    this.width$.subscribe((width:number) => {
      if (!width) return;
      this.render.setStyle(this.el.nativeElement, 'width', width + 'px');
    });
  }

  get width() {
    return this.rect.width;
  }

  private generateTemplate() {
    this.render.appendChild(this.el.nativeElement, this.item.element);
  }
}
