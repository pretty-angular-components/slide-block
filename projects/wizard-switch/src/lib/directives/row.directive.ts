import { Directive, ElementRef, Renderer2, Input, AfterViewInit, OnInit } from '@angular/core';
import { AnimationBuilder } from '@angular/animations';
import { Observable } from 'rxjs';

import { Animation } from '../animate/animate.component';
import { Styles } from '../animate/config';
import { WizardSwitchService } from '../services/wizard-switch.service';

@Directive({
  selector: '[kpRow]'
})
export class RowDirective extends Animation implements AfterViewInit, OnInit {
  @Input() private width: Observable<number>;
  @Input() private startAnimation: Observable<number>;
  @Input() private autoResponsive = false;

  protected styleName = Styles.width;

  constructor(public builder: AnimationBuilder,
              private el: ElementRef,
              private render: Renderer2,
              public switchService: WizardSwitchService) {
    super(builder, switchService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    this.width.subscribe((width: number) => {
      if (!width) return;
      this.render.setStyle(this.el.nativeElement, 'width', width + 'px');
    });

    this.startAnimation.subscribe(width => {
      if (!this.autoResponsive) return;
      this.animate(this.element, width);
    });
  }

  get element() {
    return this.el.nativeElement;
  }
}
