import { Directive, Input, Renderer2, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[kpItem]'
})
export class ItemDirective implements AfterViewInit {
  @Input() private _index;
  maxZIndex = 1000;

  constructor(private render: Renderer2, private el: ElementRef) { }

  ngAfterViewInit() {
    this.render.setStyle(this.el.nativeElement, 'z-index', `${this.maxZIndex - this._index}`);
  }

  get index() {
    return this._index;
  }

  get elem() {
    return this.el.nativeElement;
  }
}
