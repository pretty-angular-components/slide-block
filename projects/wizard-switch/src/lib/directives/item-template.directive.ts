import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[kpItemTemplate]'
})
export class ItemTemplateDirective {
  @Input() kpItemTemplate = '';
  @Input() control = 'Control';
  @Input() disabled = false;

  constructor(private el: ElementRef) { }

  get element() {
    return this.el.nativeElement;
  }

  get isIcon() {
    return typeof this.control === 'object';
  }
}
