import {
  Component,
  OnInit,
  QueryList,
  ViewChildren,
  AfterViewInit,
  OnDestroy,
  Input,
  ContentChildren, Output, EventEmitter
} from '@angular/core';
import { AnimationBuilder } from '@angular/animations';

import { BehaviorSubject, Subject, Subscription } from 'rxjs';

import { ElementDirective } from './directives/element.directive';
import { ItemDirective } from './directives/item.directive';
import { WizardSwitchService } from './services/wizard-switch.service';
import { TriggerService } from './services/trigger.service';
import { ControlDirective } from './directives/control.directive';
import { Events, EventStates } from './enums/events';
import { ItemTemplateDirective } from './directives/item-template.directive';
import { Animation } from './animate/animate.component';
import { Styles } from './animate/config';
import { SwitchInterface } from './models-interfaces/switch';
import { ControlOptionInterface } from './models-interfaces/controlOption';

@Component({
  selector: 'kp-wizard-switch',
  templateUrl: './wizard-switch.component.html',
  styleUrls: ['./wizard-switch.component.scss'],
  providers: [WizardSwitchService]
})
export class WizardSwitchComponent extends Animation implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(ElementDirective) private templateElements: QueryList<ElementDirective>;
  @ViewChildren(ItemDirective) private itemElements: QueryList<ItemDirective>;
  @ViewChildren(ControlDirective) private controlElements: QueryList<ControlDirective>;
  @ContentChildren(ItemTemplateDirective) itemTemplateElements: QueryList<ItemTemplateDirective>;

  @Input() uniformWidth = true;
  @Input() autoResponsive = false;
  @Input() controlStyle = 'kp-wizard-control';
  @Input() name = null;

  @Output() onStartAnimation: EventEmitter<void> = new EventEmitter<void>();
  @Output() onEndAnimation: EventEmitter<void> = new EventEmitter<void>();

  protected styleName = Styles.translateX;

  startAnimation$ = new Subject<number>();
  maxSwitchWidth$ = new BehaviorSubject<number>(0);
  maxTemplateWidth$ = new BehaviorSubject<number>(0);

  private openIndex = 0;
  private eventState: EventStates;
  private skipEventNum = 0;
  private compSubscription = new Subscription();

  constructor(public builder: AnimationBuilder,
              public switchService: WizardSwitchService,
              private triggerService: TriggerService) {
    super(builder, switchService);
  }

  ngOnInit() {
    this.checkAutoResponsive(); // This need to off autoResponsive then template width not unique,
                                // because autoResponsive must use then uniformWidth === false
    super.ngOnInit();

    this.compSubscription.add(
      this.switchService.getIndex().subscribe(index => {
        this.openIndex = index === this.openIndex ? null : index;
        if (this.eventState === EventStates.start) {
          this.skipEventNum++;
          this.onEndAnimation.emit();
        }
        this.eventState = EventStates.click;
        this.startAnimate();
      })
    );

    this.compSubscription.add(
      this.switchService.getEmitter().subscribe(event => {
        if (this.eventState) {
          this.generateEvent(event);
        }
      })
    );

    this.compSubscription.add(
      this.triggerService.getTriggerSpecOptions()
        .subscribe((option: ControlOptionInterface) => {
          this.triggeredSpecOptions(option);
        })
    );

    this.name = this.name || Math.random().toString(36).substr(2, 5);
  }

  ngOnDestroy() {
    this.compSubscription.unsubscribe();
    this.startAnimation$.complete();
    this.maxSwitchWidth$.complete();
    this.startAnimation$.complete();
  }

  ngAfterViewInit() {
    this.initState();
  }

  private initState() {
    this.itemElements.forEach((item, index) => {
      this.animate(item.elem, this.countOffset(index), false, true);
    });

    this.maxSwitchWidth$.next(this.autoResponsive ? this.openSwitchWidth : this.maxSwitchWidth);

    if (this.uniformWidth) {
      this.maxTemplateWidth$.next(this.maxTemplateWidth);
    }
  }

  private get templateWidths() {
    return this.templateElements.map(item => item.width);
  }

  private get controlsWidth() {
    return this.controlElements.reduce((width, control: ControlDirective) => {
      return width + control.width;
    }, 0);
  }

  private get maxTemplateWidth() {
    return this.templateWidths.reduce((acc: number, val: number) => {
      return val > acc ? val : acc;
    }, 0);
  }

  private get maxSwitchWidth() {
    return this.controlsWidth + this.maxTemplateWidth;
  }

  private get openSwitchWidth() {
    return this.openIndex !== null
      ? this.controlsWidth + this.templateWidths[this.openIndex]
      : this.controlsWidth;
  }

  private countOffset(index: number): number {
    const widthArray = this.uniformWidth
      ? Array(this.templateElements.length).fill(this.maxTemplateWidth)
      : [...this.templateWidths];

    if (this.openIndex !== null) {
      widthArray[this.openIndex] = 0;
    }

    return widthArray.slice(0, index + 1).reduce((sum, current) => sum + current, 0);
  }

  private startAnimate() {
    this.startAnimation$.next(this.openSwitchWidth);
    this.itemElements.forEach((item, index) => {
      const isEvents = index === 0; // Start event on first item (we must ignore another items)
      this.animate(item.elem, this.countOffset(index), isEvents);
    });
  }

  private checkAutoResponsive() {
    if (this.uniformWidth && this.autoResponsive) {
      this.autoResponsive = false;
    }
  }

  private generateEvent(event: Events) {
    if (event === Events.onStartAnimation && this.eventState === EventStates.click) {
      this.generateAnimationStartEvent();
    } else if (event === Events.onEndAnimation && this.eventState === EventStates.start) {
      this.generateAnimationEndEvent();
    }
  }

  private generateAnimationStartEvent() {
    this.eventState = EventStates.start;
    this.onStartAnimation.emit();
  }

  private generateAnimationEndEvent() {
    if (this.skipEventNum) {
      this.skipEventNum--;
    } else {
      this.eventState = EventStates.end;
      this.onEndAnimation.emit();
    }
  }

  private triggeredSpecOptions(option: ControlOptionInterface) {
    if (option.switchName && option.switchName !== this.name) return;

    const lastIndex = this.templateElements.length - 1;
    const switchData: SwitchInterface = {name: this.name, lastIndex, openIndex: this.openIndex};
    this.triggerService.triggeredSpecOptions(option, switchData);
  }
}
