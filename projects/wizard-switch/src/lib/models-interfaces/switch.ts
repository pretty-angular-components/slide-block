export interface SwitchInterface {
  name: string;
  lastIndex: number;
  openIndex: number;
}
