import { Trigger } from '../enums/trigger';

export interface ControlOptionInterface {
  switchName: string[];
  value?: string | number | boolean;
  type?: Trigger;
}
