import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { WizardSwitchComponent } from './wizard-switch.component';
import { ControlDirective } from './directives/control.directive';
import { ElementDirective } from './directives/element.directive';
import { ItemDirective } from './directives/item.directive';
import { RowDirective } from './directives/row.directive';
import { ItemTemplateDirective } from './directives/item-template.directive';

@NgModule({
  declarations: [
    WizardSwitchComponent,
    ControlDirective,
    ElementDirective,
    ItemDirective,
    RowDirective,
    ItemTemplateDirective
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FontAwesomeModule
  ],
  exports: [
    WizardSwitchComponent,
    ItemTemplateDirective
  ]
})
export class WizardSwitchModule { }
