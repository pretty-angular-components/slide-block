import { TestBed } from '@angular/core/testing';

import { WizardSwitchService } from './wizard-switch.service';

describe('WizardSwitchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WizardSwitchService = TestBed.get(WizardSwitchService);
    expect(service).toBeTruthy();
  });
});
