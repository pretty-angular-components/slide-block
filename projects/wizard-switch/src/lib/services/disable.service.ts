import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ControlOptionInterface } from '../models-interfaces/controlOption';

@Injectable({
  providedIn: 'root'
})
export class DisableService {
  private option = new Subject<ControlOptionInterface>();
  private disableAllElem = new Subject<ControlOptionInterface>();

  constructor() { }

  setChangeDisabledOption(value: string, switchName: string[]) {
    this.option.next({value, switchName});
  }

  getChangeDisabledOption() {
    return this.option.asObservable();
  }

  disableAll(switchName: string[]) {
    this.disableAllElem.next({value: false, switchName});
  }

  enableAll(switchName: string[]) {
    this.disableAllElem.next({value: true, switchName});
  }

  getChangeDisabledAll() {
    return this.disableAllElem.asObservable();
  }
}
