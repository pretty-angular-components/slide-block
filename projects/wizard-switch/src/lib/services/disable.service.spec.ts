import { TestBed } from '@angular/core/testing';

import { DisableService } from './disable.service';

describe('DisableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisableService = TestBed.get(DisableService);
    expect(service).toBeTruthy();
  });
});
