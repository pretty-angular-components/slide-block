import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Trigger } from '../enums/trigger';
import { SwitchInterface } from '../models-interfaces/switch';
import { ControlOptionInterface } from '../models-interfaces/controlOption';

@Injectable({
  providedIn: 'root'
})
export class TriggerService {
  private trigger = new Subject<any>();
  private triggerSpecOptions = new Subject<any>();

  constructor() { }

  getTrigger() {
    return this.trigger.asObservable();
  }

  getTriggerSpecOptions() {
    return this.triggerSpecOptions.asObservable();
  }

  setTrigger(option: ControlOptionInterface) {
    this.trigger.next(option);
  }

  triggerControl(value: string, switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.trigger, switchName, value});
  }

  closeAll(switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.closeAll, switchName});
  }

  openFirst(switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.openFirst, switchName});
  }

  openLast(switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.openLast, switchName});
  }

  openNext(switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.openNext, switchName});
  }

  openPrev(switchName: string[] = null) {
    this.triggerSpecOptions.next({type: Trigger.openPrev, switchName});
  }

  triggeredSpecOptions(option: ControlOptionInterface, switchData: SwitchInterface) {
    if (option.switchName &&  !this.findName(option.switchName, switchData.name)) return;

    switch (option.type) {
      case Trigger.trigger:
        const triggerOptions: ControlOptionInterface = {value: option.value, switchName: [switchData.name]};
        this.setTrigger(triggerOptions);
        break;
      case Trigger.closeAll:
        if (switchData.openIndex !== null) {
          const closeAllOptions: ControlOptionInterface = {value: switchData.openIndex, switchName: [switchData.name]};
          this.setTrigger(closeAllOptions);
        }
        break;
      case Trigger.openFirst:
        if (switchData.openIndex !== 0) {
          const openFirstOptions: ControlOptionInterface = {value: 0, switchName: [switchData.name]};
          this.setTrigger(openFirstOptions);
        }
        break;
      case Trigger.openLast:
        if (switchData.openIndex !== switchData.lastIndex) {
          const openLastOptions: ControlOptionInterface = {value: switchData.lastIndex, switchName: [switchData.name]};
          this.setTrigger(openLastOptions);
        }
        break;
      case Trigger.openNext:
        if (switchData.openIndex !== null && switchData.openIndex !== switchData.lastIndex) {
          const openNextOptions: ControlOptionInterface = {value: switchData.openIndex + 1, switchName: [switchData.name]};
          this.setTrigger(openNextOptions);
        }
        break;
      case Trigger.openPrev:
        if (switchData.openIndex !== null && switchData.openIndex !== 0) {
          const openPrevOptions: ControlOptionInterface = {value: switchData.openIndex - 1, switchName: [switchData.name]};
          this.setTrigger(openPrevOptions);
        }
        break;
    }
  }

  private findName(names: string[], findName: string) {
    return !!names.find((name: string) => name === findName);
  }
}
