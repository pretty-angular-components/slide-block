import { Injectable } from '@angular/core';
import { DisableService } from './disable.service';
import { TriggerService } from './trigger.service';

@Injectable({
  providedIn: 'root'
})
export class ControlService {

  constructor(private disableService: DisableService,
              private triggerService: TriggerService) { }

  private normalizeSwitchNames(switchName?: string | string[]) {
    if (typeof switchName === 'string') return [switchName];
    if (Array.isArray(switchName)) return switchName;

    return null;
  }

  /**
   * Disable service
   */

  disableAll(switchName?: string | string[]) {
    this.disableService.disableAll(this.normalizeSwitchNames(switchName));
  }

  enableAll(switchName?: string | string[]) {
    this.disableService.enableAll(this.normalizeSwitchNames(switchName));
  }

  disable(value: string, switchName?: string | string[]) {
    this.disableService.setChangeDisabledOption(value, this.normalizeSwitchNames(switchName));
  }

  /**
   * Trigger service
   */

  trigger(value: string, switchName?: string | string[]) {
    this.triggerService.triggerControl(value, this.normalizeSwitchNames(switchName));
  }

  closeAll(switchName?: string | string[]) {
    this.triggerService.closeAll(this.normalizeSwitchNames(switchName));
  }

  openFirst(switchName?: string | string[]) {
    this.triggerService.openFirst(this.normalizeSwitchNames(switchName));
  }

  openLast(switchName?: string | string[]) {
    this.triggerService.openLast(this.normalizeSwitchNames(switchName));
  }

  openNext(switchName?: string | string[]) {
    this.triggerService.openNext(this.normalizeSwitchNames(switchName));
  }

  openPrev(switchName?: string | string[]) {
    this.triggerService.openPrev(this.normalizeSwitchNames(switchName));
  }
}
