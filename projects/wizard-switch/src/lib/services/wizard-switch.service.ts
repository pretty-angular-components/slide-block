import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WizardSwitchService {
  // private openIndex = new BehaviorSubject<number>(0);
  private pushIndex = new Subject<number>();
  private event: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  setIndex(index: number) {
    this.pushIndex.next(index);
    // const openIndex = this.openIndex.value === index ? null : index;
    // this.openIndex.next(openIndex);
  }

  // getOpenIndex() {
  //   return this.openIndex.asObservable();
  // }

  getIndex() {
    return this.pushIndex.asObservable();
  }

  emitEvent(name: string) {
    this.event.emit(name);
  }

  getEmitter() {
    return this.event;
  }
}
