import { OnInit, Input } from '@angular/core';
import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, AnimationStyleMetadata, style } from '@angular/animations';

import { AnimateConfig, AnimateOptions } from './animate-config';
import { Styles } from './config';
import { WizardSwitchService } from '../services/wizard-switch.service';
import { Events } from '../enums/events';

export class Animation implements OnInit {
  @Input() animateOptions: AnimateOptions;
  animateConfig: AnimateConfig;
  protected player: AnimationPlayer;
  protected styleName: Styles;

  constructor(public builder: AnimationBuilder, public switchService: WizardSwitchService) {}

  ngOnInit() {
    this.animateConfig = new AnimateConfig(this.animateOptions);
  }

  protected animate(el, param: number, isEvents = false, instantly = false) {
    const animation: AnimationFactory = this.builder.build([
      animate(instantly ? this.animateConfig.defaultTiming : this.animateConfig.timing, this.getAnimationStyle(param))
    ]);

    this.player = animation.create(el);

    if (isEvents) {
      this.player.onStart(() => this.generateStartAnimationEvent());
      this.player.play();
      this.player.onDone(() => this.generateEndAnimationEvent());
    } else {
      this.player.play();
    }
  }

  private getAnimationStyle(param: any): AnimationStyleMetadata {
    const styles = {
      translateX: style({transform: `translateX(-${param}px)`}),
      width: style({width: `${param}px`})
    };

    return styles[this.styleName];
  }

  private generateStartAnimationEvent() {
    if (this.styleName === Styles.translateX) {
      this.switchService.emitEvent(Events.onStartAnimation);
    }
  }

  private generateEndAnimationEvent() {
    if (this.styleName === Styles.translateX) {
      this.switchService.emitEvent(Events.onEndAnimation);
    }
  }
}
