import { Smooth } from './config';

export interface AnimateOptions {
  delay ?: string | number;
  duration ?: string | number;
  easing ?: Smooth | string;
}

export class AnimateConfig {
  private _animation: AnimateOptions;

  constructor(animation: AnimateOptions = {}) {
    this._animation = this.defaultAnimation;
    this.theme = animation;
  }

  get defaultAnimation(): AnimateOptions {
    return {
      delay: 0,
      duration: '250ms',
      easing: Smooth.easeIn
    };
  }

  get timing() {
      return `${this._animation.duration} ${this._animation.delay ? this._animation.delay + ' ' : ''}${this._animation.easing}`;
  }

  get defaultTiming() {
    return '0ms';
  }

  set theme(animation: AnimateOptions) {
    if (animation.delay) {
      this.checkCorrectTiming(animation.delay, 'delay');
    }

    if (animation.duration || animation.duration === 0) {
      this.checkCorrectTiming(animation.duration, 'duration');
    }

    if (animation.easing) {
      if (animation.easing.includes('cubic-bezier', 0)) {
        this._animation.easing = animation.easing;
      } else {
        this._animation.easing = Smooth[animation.easing] || this.defaultAnimation.easing;
      }
    }
  }

  private checkCorrectTiming(timing: string | number, name: string): void {
    if (typeof timing === 'number') {
      this._animation[name] = timing + 'ms';
    }

    if (typeof timing === 'string') {
      const timingRegexp = /^(\d{1,4}(ms|s)|(\d{1,2}(.\d{1,2})?s))\b/;
      if (timingRegexp.test(timing)) {
        this._animation[name] = timing;
      }
    }
  }
}
