export enum Smooth {
  ease = 'ease',
  easeIn = 'ease-in',
  easeOut = 'ease-out',
  easeInOut = 'ease-in-out',
  linear = 'linear',
}

export enum Styles {
  translateX = 'translateX',
  width = 'width'
}
