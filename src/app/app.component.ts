import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { ControlService } from 'wizard-switch';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private service: ControlService,
              private fb: FormBuilder) {}
  faCoffee = faCoffee;
  eventName = 'No event';
  form;

  ngOnInit() {
    this.form = this.fb.group({
      login: 'No data',
      password: 'No data',
      gender: 'man'
    });
  }

  changeDisabled() {
    this.service.disable('login');
  }

  disableAll() {
    this.service.disableAll(['second', 'third']);
  }

  enableAll() {
    this.service.enableAll(['second', 'first']);
  }

  onStart() {
    this.eventName = 'Start animation';
  }

  onEnd() {
    this.eventName = 'End animation';
  }

  trigger() {
    this.service.trigger('login', 'first');
  }

  openFirst() {
    this.service.openFirst();
  }

  openLast() {
    this.service.openLast('second');
  }

  openNext() {
    this.service.openNext();
  }

  openPrev() {
    this.service.openPrev();
  }

  closeAll() {
    this.service.closeAll();
  }
}
