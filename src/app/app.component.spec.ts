import { TestBed, async } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { AppComponent } from './app.component';
import { DisableService } from 'wizard-switch';
import {Subject} from 'rxjs';

describe('AppComponent', () => {
  const formBuilder: FormBuilder = new FormBuilder();
  // class DisableServiceStub  {
  //   index: new Subject
  //   disableAllIndexes: '',
  //   setChangeDisabledIndex: (index: number) => {},
  //   getChangeDisabledIndex: () => {},
  //   disableAll: () => {},
  //   enableAll: () => {},
  //   getDisableAll: () => {}
  // };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        // { provide: DisableService, useClass: DisableServiceStub },
        { provide: FormBuilder, useValue: formBuilder }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  // it(`should have as title 'slide-block'`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('slide-block');
  // });
  //
  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to slide-block!');
  // });
});
